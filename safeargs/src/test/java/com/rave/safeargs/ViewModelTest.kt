package com.rave.safeargs

import com.rave.safeargs.model.MathRepo
import com.rave.safeargs.model.NetworkModule
import com.rave.safeargs.view.result.ResultState
import com.rave.safeargs.viewmodel.MathViewModel
import io.mockk.coEvery
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class ViewModelTest {

    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()
    private val repo = MathRepo(NetworkModule.providesQuoteService(NetworkModule.providesRetrofit()))
    private val viewModel = MathViewModel(repo)

    /*@BeforeEach
    fun setUp() {
        mockkObject(MathRepo())
    }*/

    @Test
    @DisplayName("Testing that num1 state is updated when new number is passed in")
    fun updateNum1() {
        // Given
        val newNum: String = "123"

        // When
        viewModel.updateNum1(newNum)

        // Then
        Assertions.assertEquals(newNum, viewModel.state.value.num1)
    }

    @Test
    @DisplayName("Testing that num2 state is updated when new number is passed in")
    fun updateNum2() {
        // Given
        val newNum: String = "123"

        // When
        viewModel.updateNum2(newNum)

        // Then
        Assertions.assertEquals(newNum, viewModel.state.value.num2)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun calculateResult() = runTest(coroutinesTestExtension.testDispatcher) {
        // Given
        val num1 = "123"
        val num2 = "321"
        val operator = "+"
        val result = "444"
        val stateUpdates = mutableListOf<ResultState>()
        viewModel.updateNum1(num1)
        viewModel.updateNum2(num2)
        viewModel.updateOperator(operator)
        coEvery { repo.evaluateExpression("$num1$operator$num2") } coAnswers { result }

        val job = launch {
            viewModel.state.toList(stateUpdates)
        }

        // When
        viewModel.calculateResult()

        // Then
        Assertions.assertEquals(result, stateUpdates.last().result)
        job.cancel()
    }
}
