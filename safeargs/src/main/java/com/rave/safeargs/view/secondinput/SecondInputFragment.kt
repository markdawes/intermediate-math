package com.rave.safeargs.view.secondinput

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.rave.safeargs.ui.theme.IntermediateMathTheme

/**
 * Fragment for screen that takes second input.
 *
 * @constructor Create empty First input fragment
 */
class SecondInputFragment : Fragment() {
    private val args: SecondInputFragmentArgs by navArgs()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                var textFieldState by remember { mutableStateOf("") }
                IntermediateMathTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            TextField(
                                value = textFieldState,
                                label = {
                                    Text(text = "Enter second number")
                                },
                                onValueChange = {
                                    textFieldState = it
                                },
                                singleLine = true
                            )
                            Button(onClick = {
                                val action = SecondInputFragmentDirections
                                    .actionSecondInputFragmentToResultFragment(
                                        args.num1,
                                        args.operator,
                                        textFieldState
                                    )
                                findNavController().navigate(action)
                            }) {
                                Text(text = "Next")
                            }
                        }
                    }
                }
            }
        }
    }
}
