package com.rave.safeargs.model

import javax.inject.Inject

/**
 * Repository to evaluate math expressions.
 *
 * @property service
 * @constructor Create empty Math repo
 */
class MathRepo @Inject constructor(private val service: MathService) {

    /**
     * Evaluates mathematical expression passed in.
     *
     * @param expr
     * @return
     */
    suspend fun evaluateExpression(expr: String): String {
        return service.evaluateExpression(expr).toString()
    }
}
