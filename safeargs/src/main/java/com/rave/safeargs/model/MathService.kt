package com.rave.safeargs.model

import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Defines the endpoints needed.
 *
 * @constructor Create empty Math service
 */
interface MathService {
    @GET(".")
    suspend fun evaluateExpression(@Query("expr") expr: String): Double
}
