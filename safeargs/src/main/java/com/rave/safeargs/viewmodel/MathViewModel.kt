package com.rave.safeargs.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.safeargs.model.MathRepo
import com.rave.safeargs.view.result.ResultState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * Viewmodel to hold values for evaluated expression.
 *
 * @property repo
 * @constructor Create empty Math view model
 */
@HiltViewModel
class MathViewModel @Inject constructor(private val repo: MathRepo) : ViewModel() {

    private val _state = MutableStateFlow(ResultState())
    val state get() = _state.asStateFlow()

    /**
     * Update num1.
     *
     * @param num
     */
    fun updateNum1(num: String) {
        _state.update { state -> state.copy(num1 = num) }
    }

    /**
     * Update num2.
     *
     * @param num
     */
    fun updateNum2(num: String) {
        _state.update { state -> state.copy(num2 = num) }
    }

    /**
     * Update operator.
     *
     * @param operator
     */
    fun updateOperator(operator: String) {
        _state.update { state -> state.copy(operator = operator) }
    }

    /**
     * Calculate sum.
     *
     */
    fun calculateResult() {
        _state.update { state -> state.copy(isEvaluating = true) }
        viewModelScope.launch {
            val expr: String = _state.value.run { "$num1$operator$num2" }
            val result: String = repo.evaluateExpression(expr)
            _state.update { state -> state.copy(isEvaluating = false, result = result) }
        }
    }
}
