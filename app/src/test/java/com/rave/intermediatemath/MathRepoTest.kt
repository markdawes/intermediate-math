package com.rave.intermediatemath

import com.rave.intermediatemath.model.MathRepo
import com.rave.intermediatemath.model.MathService
import com.rave.intermediatemath.model.NetworkModule
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class MathRepoTest {

    @RegisterExtension
    private val extension = CoroutinesTestExtension()
    private val mockService = mockk<MathService>()
    private val repo = MathRepo(mockService)

    @BeforeEach
    fun beforeEach() {
        mockkObject(NetworkModule)
        every { NetworkModule.providesQuoteService(NetworkModule.providesRetrofit()) } answers { mockService }
    }

    @Test
    @DisplayName("Testing that we can evaluate expressions")
    fun testEvaluateExpression() = runTest(extension.testDispatcher) {
        // Given
        val expr = "2+2"
        val result = 4.0
        coEvery { mockService.evaluateExpression(expr) } coAnswers { result }

        // When
        val testResult = repo.evaluateExpression(expr)

        // Then
        Assertions.assertEquals(result.toString(), testResult)
    }
}
