package com.rave.intermediatemath.view.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.rave.intermediatemath.ui.theme.IntermediateMathTheme
import com.rave.intermediatemath.viewmodel.MathViewModel

/**
 * Fragment that shows the evaluated expression.
 *
 * @constructor Create empty Result fragment
 */
class ResultFragment : Fragment() {

    private val num1 by lazy { arguments?.getString("num1") }
    private val arg by lazy { arguments?.getString("operator") }
    private val num2 by lazy { arguments?.getString("num2") }

    private val viewModel: MathViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by viewModel.state.collectAsState()
                val operator = when(arg) {
                    "0" -> "+"
                    "1" -> "-"
                    "2" -> "*"
                    "3" -> "/"
                    else -> ""
                }
                viewModel.updateNum1(num1!!)
                viewModel.updateNum2(num2!!)
                viewModel.updateOperator(operator)
                viewModel.calculateResult()
                IntermediateMathTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Text(text = "$num1 $operator $num2 = ${state.result}")
                        }
                    }
                }
            }
        }
    }
}
