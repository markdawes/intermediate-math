package com.rave.intermediatemath.view.result

/**
 * Data for result fragment.
 *
 * @property num1
 * @property num2
 * @property operator
 * @property result
 * @property isEvaluating
 * @constructor Create empty Result state
 */
data class ResultState(
    val num1: String = "",
    val num2: String = "",
    val operator: String = "",
    val result: String = "",
    val isEvaluating: Boolean = false
)
