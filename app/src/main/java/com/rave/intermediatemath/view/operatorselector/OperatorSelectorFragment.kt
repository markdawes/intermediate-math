package com.rave.intermediatemath.view.operatorselector

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.toSize
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.rave.intermediatemath.R
import com.rave.intermediatemath.ui.theme.IntermediateMathTheme

/**
 * Fragment for selecting operator.
 *
 * @constructor Create empty Operator selector fragment
 */
class OperatorSelectorFragment : Fragment() {
    private val num1 by lazy { arguments?.getString("num1") }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state = rememberMenuState()
                IntermediateMathTheme {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Box {
                                OutlinedTextField(
                                    value = state.value,
                                    onValueChange = {},
                                    label = { Text(text = "Select Operator") },
                                    modifier = Modifier.onGloballyPositioned {
                                        state.onSize(it.size.toSize())
                                    }
                                )
                                DropdownMenu(
                                    expanded = state.enabled,
                                    modifier = Modifier.width(with(LocalDensity.current) {
                                        state.size.width.toDp()
                                    }),
                                    onDismissRequest = {
                                        state.onEnabled(false)
                                    }) {
                                    state.items.forEachIndexed { index, s ->
                                        DropdownMenuItem(
                                            text = { Text(text = s) },
                                            onClick = {
                                                state.onSelectedIndex(index)
                                                state.onEnabled(false)
                                            }
                                        )
                                    }
                                }
                            }
                            Button(onClick = {
                                val args : Bundle = bundleOf(
                                    "num1" to num1,
                                    "operator" to state.selectedIndex.toString()
                                )
                                findNavController().navigate(
                                    R.id.action_operatorSelectorFragment_to_secondInputFragment,
                                    args
                                )
                            }) { Text(text = "Next") }
                        }
                    }
                }
            }
        }
    }
}
