package com.rave.intermediatemath.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rave.intermediatemath.R
import com.rave.intermediatemath.viewmodel.MathViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Entry point for the application.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : FragmentActivity() {
    @Suppress("LateinitUsage")
    private lateinit var navController: NavController

    @Suppress("UnusedPrivateMember")
    private val mathViewModel by viewModels<MathViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }
}
