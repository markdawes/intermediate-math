package com.rave.intermediatemath.view.operatorselector

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.geometry.Size

/**
 * Holds state of the dropdown menu.
 *
 * @constructor Create empty Drop menu state
 */
class DropMenuState {

    var enabled by mutableStateOf(true)
    var value by mutableStateOf("")
    var selectedIndex by mutableStateOf(-1)
    var size by mutableStateOf(Size.Zero)

    val items = listOf<String>("+", "-", "*", "/")

    /**
     * On enabled.
     *
     * @param newValue
     */
    fun onEnabled(newValue: Boolean) {
        enabled = newValue
    }

    /**
     * On selected index.
     *
     * @param newValue
     */
    fun onSelectedIndex(newValue: Int) {
        selectedIndex = newValue
        value = items[selectedIndex]
    }

    /**
     * On size.
     *
     * @param newValue
     */
    fun onSize(newValue: Size) {
        size = newValue
    }
}

@Composable
fun rememberMenuState() = remember {
    DropMenuState()
}
