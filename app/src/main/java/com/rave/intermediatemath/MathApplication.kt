package com.rave.intermediatemath

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Math application.
 *
 * @constructor Create empty Math application
 */
@HiltAndroidApp
class MathApplication : Application()
